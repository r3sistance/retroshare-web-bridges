var langs = {
  es: {
    translation : {
      "title": "<span class='mLocationName'></span>",
      "info-message": "Distribución descentralizada de contenidos de <span class='mLocationName'></span>",

      "info-message-sub": "Recuerda que tu dispositivo local requiere tener instalado <a href='https://retroshare.cc' target='_blank'>retroshare</a>.<br>Los dispositivos se conocen el uno al otro y lo hacen intercambiando certificados.<br>Por eso <b>hay que llevar a cabo las acciones 1 y 2</b> que se muestran a continuación:<br>",

      "copy-key-card-title" : "<b>1. Conoce <span class='mLocationName'></span>! Guarda su certificado en tu dispositivo</b>",

      "paste-key-card-title" : "<b>2. Date a conocer! Envia el certificado de tu dispositivo a <span class='mLocationName'></span>",

      "btn-copy" : "Copiar",
      "btn-submit" : "Enviar",
    }
  },
  ca: {
    translation : {
      "title": "<span class='mLocationName'></span>",
      "info-message": "Distribució descentralitzada de continguts de <span class='mLocationName'></span>",
      "info-message-sub": "Recorda que el teu dispositiu local requereix tenir instal·lat <a href='https://retroshare.cc' target='_blank'>retroshare</a>.<br>Els dispositius s'han de conèixer l'un a l'altre i ho faran intercanviant certificats.<br>Per això <b>calen dur a terme les accions 1 i 2</b> que es mostren a continuació:<br>",

      "copy-key-card-title" : "<b>1. Coneix <span class='mLocationName'></span>! Guarda el seu certificat en el teu dispositiu</b>",

      "paste-key-card-title" : "<b>2. Dona't a conèixer! Envia el certificat del teu dispositiu a <span class='mLocationName'></span>",

      "btn-copy" : "Copiar",
      "btn-submit" : "Enviar",
    }
  },
  en : {
    translation : {
      "title": "<span class='mLocationName'></span>",
      "info-message": "Decentralized distribution of contents of <span class='mLocationName'></span>",
      "info-message-sub": "Remember that your local device requires <a href='https://retroshare.cc' target='_blank'>retroshare</a> to be installed.<br>The devices need to know from each other, they do so exchanging their certificates.<br>That's why <b>actions 1 and 2 are needed to be done</b> and they are displayed below:<br>",

      "copy-key-card-title" : "<b>1. Meet <span class='mLocationName'></span>! Save its certificate on your device</b>",

      "paste-key-card-title" : "<b>2. Make yourself known! Send your device's certificate to <span class='mLocationName'></span>",

      "btn-copy" : "Copy",
      "btn-submit" : "Submit",
    }
  }
}
