// Global scripts for web ui

// Set a request to json api
function rsJsonApiRequest(path, data, callback, errrCb, method)
{
  var method = method || "POST";
  console.log("rsJsonApiRequest(path, data, callback)", path, data, callback)
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function()
  {
    if(xhr.readyState === 4)
    {
      if (xhr.status === 200)
      {
        console.log( path, "callback", xhr.status,
               xhr.responseText.replace(/\s+/g, " ").
                substr(0, 60).replace(/\r?\n|\r/g," ") )
        if(typeof(callback) === "function") callback(xhr.responseText);
      }
      else
      {
        if(typeof(errrCb) === "function") errrCb(xhr);
      }
    }
  }
  xhr.open(method, rsJsonApiUrl + path, true);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(data);
}



// Ui utils
function createElementFromHTML(htmlString)
{
  var div = document.createElement('div');
  div.innerHTML = htmlString.trim();

  // Change this to div.childNodes to support multiple top-level nodes
  return div.firstChild;
}

// Return date on human readable format from epoch format time
function getDateFromEpoch (epoch)
{
  return new Date(epoch * 1000)
}

// Url encode an url
function sanitizeUrl (url)
{
  return encodeURI(url)
}

// Create a Retroshare link for forums or channels
// Type: channel, forum...
// Name: name of the resource
// Id: Id of the channel, or forum
// msgId: (OPTIONAL) msg id inside the channel or forum
function getRSLinkChannelForum(type,name,id,msgId)
{
  var msgId = msgId || "";
  var link = "retroshare://"+type+"?name="+sanitizeUrl(name)+"&id=" +id +msgId
  return link
}

// Send RS file object
function getRSLinkFile (f)
{
  var link = "retroshare://file?name="+sanitizeUrl(f.mName)+"&size="+f.mSize+"&hash="+f.mHash
  return link
}
