# Auto join Retroshare Network

This web bridge is built to make easier to people to login on to a Retroshare network. If the node is propagating **public content** it helps to people to access to it by adding their own key to the node and start to get in the network.

This will facilitate to people to enter on public RS networks.

See more:

* https://gitlab.com/RepositorioDeCultura/Tier1
* https://gitlab.com/pedrolab/retroshare-relay-guide


### Configure to protect

On the original repo, they use [login.sh](login.sh) to get and show info about the node (such as node name). It could be done by calling a script made in lua on `nginx.conf` reverse proxy configuration, as [suggested here](https://stackoverflow.com/questions/22788236/how-can-i-manipulate-the-json-body-of-a-post-request-using-nginx-and-lua).

For more info see `nginx.conf/auto-join.nginx.conf`.

Meanwhile, what `auto-join.html` do is get the node information from a generated file from `login.sh` script. If the file doesn't exist just take the info from a hardcoded json on javascript code.

This is done on this way with the objective to **show information about the node without leak information about the other location profiles existing on it** because the call `rsLoginHelper/getLocations` return all locations info, and we want to show just one.

See the code and for more info about the issue write!

### Screenshoots

![Screenshoot](../imgs/auto-join.png)
